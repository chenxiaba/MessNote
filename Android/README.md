#Design document of Android
##Add & Save
1. At MainActivity, you can Click the Add Button to quickly add a new note;
2. The change to EditActivity, you can write you mind the come back to MainActivity by return button;
3. The note content will be auto saved.

##Display
The MainActivity is a list of notes, then you can see the summary of notes.

##Modify & Save
1. Click the note item, the change to EditActivity;
2. Modify your content;
3. Click back button and auto saved.

##Export & Backup
1. Click the Menu, select `Export`;
2. Offer a dialog to support `Export new` and `Export all` function;
2. Auto export notes to SD Card;
3. Auto Backup new notes.

>For more convenient, add a flag for `Backup` or `New` to distinguish the notes have exported.

Achieve is on progress...
##Wastepaper basket
Waiting for TODO

##Cloud Sync
Waiting for TODO.


