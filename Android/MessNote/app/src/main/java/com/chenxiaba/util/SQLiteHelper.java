package com.chenxiaba.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {
    private static final String TAG = "MainActivity";
	private static final int DB_VERSION = 2;
	private static final String DB_NAME = "messnote";
    private static final String DB_TABLE_NAME = "note";
    
    private static final String DB_TABLE_CREATE =
                "CREATE TABLE " + DB_TABLE_NAME + " (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "timestamp" + " VARCHAR(25), " +
                "content" + " TEXT);";
    
	public SQLiteHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DB_TABLE_CREATE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		if(newVersion > oldVersion){
            Log.i(TAG, "New app database version is newer then old . "+
                    Integer.toString(newVersion) +
                    "(new) > " +
                    Integer.toString(oldVersion) +
                    "(old)");

            if (oldVersion ==1 && newVersion == 2) {
                //Do as you want
                Log.i(TAG, "Update database for DB_VERSION_" + Integer.toString(newVersion));
                String sql = "ALTER TABLE " + DB_TABLE_NAME + " ADD COLUMN guid TEXT";
                db.execSQL(sql);

                sql = "ALTER TABLE " + DB_TABLE_NAME + " ADD COLUMN info TEXT";
                db.execSQL(sql);

                Log.i(TAG, "Update data base to version " + Integer.toString(newVersion) + " successfully.");
            }
		}
	}
}
