package com.chenxiaba.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @ClassName: Common
 * @Description: Offer some common function
 * @author: chenxiaba
 * @date: 2014-5-5 上午12:09:08

 */
public class Common {
	
	/**
	 * @Title: getCurrentDate
	 * @Description: TODO
	 * @return String of current date
	 */
	public static String getCurrentDate(){
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss",Locale.getDefault());
		
		return dateFormat.format(now);
	}

    public static String getDateFromStamp(long timestamp) {
        Date date = new Date();
        date.setTime(timestamp);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss",Locale.getDefault());

        return dateFormat.format(date);
    }

	public static String replaceBlank(String str) {
		String dest = "";
		if (str!=null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}


}
