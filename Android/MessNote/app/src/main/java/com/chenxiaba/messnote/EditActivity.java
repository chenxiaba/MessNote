/**  
 * Copyright © 2014公司名字. All rights reserved.
 *
 * @Title: EditActivity.java
 * @Prject: MessNote
 * @Package: com.chenxiaba.messnote
 * @Description: TODO
 * @author: chenxiaba  
 * @date: 2014-5-5 下午11:28:34
 * @version: V1.0  
 */
package com.chenxiaba.messnote;

import com.chenxiaba.util.Common;
import com.chenxiaba.util.NoteService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ShareActionProvider;
import android.widget.TextView;

/**
 * @ClassName: EditActivity
 * @Description: Add/View/Modify Note
 * @author: Administrator
 * @date: 2014-5-5 下午11:28:34
 */
public class EditActivity extends Activity {
	private static final String TAG = "EditActivity";
	
	private EditText editText = null;
	private TextView nowDate = null;
	private TextView nowTime = null;
	
	private long  noteId = 0L;
	private String noteDate = "";
	private String noteTime = "";
	private String noteContent = "";
	
	private NoteService noteService=null;

	/* (non Javadoc)
	 * @Title: onCreate
	 * @Description: For View/Edit/Save Note
	 * @param savedInstanceState
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		Log.d(TAG, "Into onCreate");
		
		//For DB
		noteService = new NoteService(this);
		
		//Get widget
		editText = (EditText) findViewById(R.id.editNote);
		nowDate = (TextView) findViewById(R.id.nowDate);
		nowTime = (TextView) findViewById(R.id.nowTime);
		
		//Get Intent data
		Intent intent = getIntent();

		String noteIdStr = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_ID);

		if (null == noteIdStr){
			noteId = 0L;
		}
		else{
			noteId = Long.parseLong(noteIdStr);
		}
		
		if(noteId == 0){
			Log.i(TAG, "For new note added.");
			
			String editTime = Common.getCurrentDate();
			String[] datetime = editTime.split(" ");
			noteDate = datetime[0];
			noteTime = datetime[1];

            //For other app share content
            String action = intent.getAction();
            String type = intent.getType();

            if (action != null && type != null
                    && Intent.ACTION_SEND.equals(action)) {
                if ("text/plain".equals(type)){
                    handleSendText(intent);
                }
                Log.d(TAG, "action: " + action + " type: " +type);
            }
		}else{
			Log.i(TAG, "For note read and modify.");
			
			noteDate = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_DATE);
			noteTime = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_TIME);
			noteContent = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_CONTENT);
			//Hide keyboard
			getWindow().setSoftInputMode(  WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		}

		nowDate.setText(noteDate);
		nowTime.setText(noteTime);
		editText.setText(noteContent);
	}
	
	/* (non Javadoc)
	 * @Title: onStart
	 * @Description: TODO
	 * @see android.app.Activity#onStart()
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		Log.v(TAG, "Into onStart");
		
		if (noteService == null) {
			Log.d(TAG, "Start to open DB");
			noteService = new NoteService(this);
		}
	}


	/* (non Javadoc)
	 * @Title: onDestroy
	 * @Description: TODO
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		Log.v(TAG, "Into onDestroy");
	}
	
	
	/* (non Javadoc)
	 * @Title: onStop
	 * @Description: TODO
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		Log.v(TAG, "Into onStop");
		
		if (noteService != null) {
			Log.d(TAG, "Start to close db");
			noteService.close();
		}
	}

	/* (non Javadoc)
	 * @Title: onRestart
	 * @Description: Resume
	 * @see android.app.Activity#onRestart()
	 */
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Log.v(TAG, "Into onRestart");
	}

	/* (non Javadoc)
	 * @Title: onPause
	 * @Description: TODO
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.v(TAG, "Into onPause");
		
		//Save Note content
		handleNote();
	}
	
	/**
	 * @Title: handleNote
	 * @Description: Update or Delete Note or Done nothing
	 * @param
	 * @return: void
	 */
	private void handleNote(){
		Log.v(TAG, "Start to handle Note.");
		
		//Get content
		String note = editText.getText().toString();
		
		//Add or Update
		if(noteId == 0){
			addNote(note);
		}
		else{
			editNote(note);
		}
	}
	
	/**
	 * @Title: editNote
	 * @Description: Handle for Edit note
	 * @param note
	 * @return: void
	 */
	private void editNote(String note){
		
		if(Common.replaceBlank(note).equals("")){
			Log.d(TAG,  "Note has not null. Need to delete it.");
			//delete 
			noteService.delNote(noteId);
			return;
		}
		
		if(!note.equals(noteContent)){
			Log.d(TAG,  "Note is modified. Need to update it");
			//update
			String editTime = Common.getCurrentDate();
			noteService.updateNote(noteId, note, editTime);
			return;
		}
		
		Log.d(TAG,  "Note has not modified. No need to update it.");
	}
	
	/**
	 * @Title: addNote
	 * @Description: Handle for add note
	 * @param note
	 * @return: void
	 */
	private void addNote(String note){
		if(!Common.replaceBlank(note).equals("")){
			Log.d(TAG, "Note has content.Need to add a new note.");
            Log.d(TAG, "Note time is: " + noteDate + " " + noteTime);

			noteId = noteService.addNote(note, noteDate + " " + noteTime);
			return;
		}
		Log.d(TAG, "Note is null. No need to add.");
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /*
    * Share your note to other app
    * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_note_share:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getDisplayText());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.info_note_share)));

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    //Handle other app send TEXT
    void handleSendText(Intent intent) {
        String shareText  = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (shareText != null) {
            noteContent = shareText;
        }
    }

    private String getDisplayText(){
        return editText.getText().toString();
    }
}
