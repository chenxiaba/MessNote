package com.chenxiaba.messnote;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.chenxiaba.util.NoteService;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @ClassName: MainActivity
 * @Description: Display a list of notes
 * @author: Administrator
 * @date: 2014-5-6 下午10:48:56
 */
public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE_ID = "com.chenxiaba.messnote.MESSAGE_ID";
	public final static String EXTRA_MESSAGE_CONTENT = "com.chenxiaba.messnote.MESSAGE_CONTENT";
	public final static String EXTRA_MESSAGE_DATE = "com.chenxiaba.messnote.MESSAGE_DATE";
	public final static String EXTRA_MESSAGE_TIME = "com.chenxiaba.messnote.MESSAGE_TIME";
	
	private static final String TAG = "MainActivity";

    private ListView notesView=null;
	private Button newNote = null;

	private NoteService noteService;
	private ArrayList<HashMap<String,String>> notelist = new ArrayList<HashMap<String,String>>();
	private CheckAdaptor listAdaptor ;
	
	//Save select position
	private Integer selectId = null;

	//IsNew
	private boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Into onCreate");

        noteService = new NoteService(this);

        //Load&Select note
        updateNotes();
        listAdaptor = new CheckAdaptor(this, notelist);
        notesView = (ListView)super.findViewById(R.id.notes);
        notesView.setAdapter(listAdaptor);
        notesView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        notesView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_DISABLED);
        notesView.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	    Log.d(TAG, "onItemClick:position=" + Integer.toString(position) );
        	    //Save selectId
				selectId = position;
				//is Edit
				isNew = false;

        	    //For Modify
        	    Intent intent = new Intent(MainActivity.this, EditActivity.class);
        	    intent.putExtra(EXTRA_MESSAGE_ID, notelist.get(position).get("id"));
        	    intent.putExtra(EXTRA_MESSAGE_CONTENT, notelist.get(position).get("content"));
        	    intent.putExtra(EXTRA_MESSAGE_DATE, notelist.get(position).get("date"));
        	    intent.putExtra(EXTRA_MESSAGE_TIME, notelist.get(position).get("time"));
				startActivity(intent);
        }});

        //New note Button
        newNote = (Button) super.findViewById(R.id.newNote);
        newNote.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isNew = true;

				//For ADD
				Intent intent = new Intent(MainActivity.this, EditActivity.class);
				startActivity(intent);
			}
		});

        //ContextMenu
        registerForContextMenu(notesView);

        //is new note or edit note
        isNew = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    /**
     * Handle for menu click
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_recommend:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_HTML_TEXT, getResources().getText(R.string.info_error_auth));
                sendIntent.setType("text/html");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.info_app_share)));
                return true;

            case R.id.action_export:
                File path = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);
                File file = new File(path, "MessNote.txt");

                try {
                    path.mkdirs();

                    FileWriter fout = new FileWriter(file);
                    for (int i=0; i < notelist.size(); i++ ){
                        HashMap<String,String> note = notelist.get(i);
                        fout.write(note.toString());
                    }

                    fout.close();

                    Toast.makeText(this, R.string.action_export + "." + file.getPath(),
                            Toast.LENGTH_SHORT).show();

                } catch (IOException e){
                    Log.e(TAG, "Error writing" + file, e);
                    Toast.makeText(this, R.string.action_export + ":" + e.toString(),
                            Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.action_about:
                return true;
            /*
            case R.id.action_evernote:
                //TODO: Add network check
                disToast(R.string.info_under_dev);

                if (mEvernoteSession.isLoggedIn()){
                    Log.d(TAG, "App no need to authenticate.Just sync note.");
                    syncNote();
                }
                else {
                    Log.d(TAG, "App need to authenticate.");
                    mEvernoteSession.authenticate(this);

                }

                return true;
            */
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /* (non Javadoc)
	 * @Title: onCreateContextMenu
	 * @Description: For select A note Operate
	 * @param menu
	 * @param v
	 * @param menuInfo
	 * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);

		getMenuInflater().inflate(R.menu.note, menu);
	}



    /* (non Javadoc)
         * @Title: onContextItemSelected
         * @Description: Menu Selected
         * @param item
         * @return
         * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
         */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		//Update selectId
		selectId = Integer.parseInt(Long.valueOf(info.id).toString());

		switch (item.getItemId()) {
		case R.id.action_delete:
            /*
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.del_notify);
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteNote();
				}
			});
			builder.setNegativeButton(R.string.cancle, null);
			builder.show();
			*/
            deleteNote();
            disToast(R.string.info_note_del);
			return true;

		default:
			return super.onContextItemSelected(item);
		}
	}

	/**
     * @Title: updateNotes
     * @Description: Get Data from database
     * @return
     * @return: ArrayList<HashMap<String,String>>
     */
    private ArrayList<HashMap<String,String>> updateNotes(){
    	//Clear
    	notelist.clear();
    	//Reload
        
        Cursor cur = noteService.allNotes();
        for  (cur.moveToFirst(); !cur.isAfterLast();cur.moveToNext())
        {
        	int contentColumn   =   cur.getColumnIndex("content");
        	int timeColumn   =   cur.getColumnIndex("timestamp");
        	int idColumn   =   cur.getColumnIndex("_id");
            String content   =   cur.getString(contentColumn);
            String timestamp   =   cur.getString(timeColumn);
            long id = cur.getLong(idColumn);
            
            //Log.d(TAG, "id:"+Integer.toString(id) + "|time:" + timestamp + "|content:" +content);
            String[] datetime = timestamp.split(" ");
            
            HashMap<String,String> map = new HashMap<String, String>();
            map.put("id", Long.toString(id));
            map.put("content", content);
            map.put("date", datetime[0]);
            map.put("time", datetime[1]);
            notelist.add(map);
        }

        return notelist;
    }


    private void deleteNote(){
        Log.v(TAG, "Start to delete note.");
        long noteId = Long.parseLong(notelist.get(Integer.parseInt(String.valueOf(selectId))).get("id").toString());
        noteService.delNote(noteId);

        //Update notelist data
        Log.v(TAG, "Update list view.");
        notelist.remove(Integer.parseInt(String.valueOf(selectId)));
        listAdaptor.notifyDataSetChanged();
    }



    private void disToast(int resId) {
        Toast remind = Toast.makeText(MainActivity.this, resId, Toast.LENGTH_SHORT);
        remind.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
        remind.show();
    }

    private void disToast(String message) {
        Toast remind = Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT);
        remind.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
        remind.show();
    }

    /* (non Javadoc)
         * @Title: onStart
         * @Description: TODO
         * @see android.app.Activity#onStart()
         */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		Log.v(TAG, "Into onStart");

		if (noteService == null) {
			Log.d(TAG, "Start to open DB");
			noteService = new NoteService(this);
		}
	}

    /* (non Javadoc)
	 * @Title: onResume
	 * @Description: Refresh Note list
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.d(TAG, "Into onResume");

		updateNotes();
		listAdaptor.notifyDataSetChanged();

		if (isNew && notelist.size()!=0) {
			notesView.setSelection(0);
		}
		else if(selectId != null){
			notesView.setSelection(selectId);
		}

		super.onResume();
	}

	/* (non Javadoc)
	 * @Title: onStop
	 * @Description: TODO
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stubi
		super.onStop();

		Log.v(TAG, "Into onStop");

		if (noteService != null) {
			Log.d(TAG, "Start to close db");
			noteService.close();
		}
	}

	/* (non Javadoc)
	 * @Title: onPause
	 * @Description: TODO
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.v(TAG, "Into onPause");
	}

	/* (non Javadoc)
	 * @Title: onDestroy
	 * @Description: TODO
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	private class CheckAdaptor extends BaseAdapter{
    	private LayoutInflater inflater ;
    	ArrayList<HashMap<String,String>> datalist;
    	
    	public CheckAdaptor(Context context , ArrayList<HashMap<String,String>> datalist){
    		super();
    		inflater = LayoutInflater.from(context);
    		this.datalist = datalist;
    	}
	    
    	@Override
    	public int getCount() {
    		return datalist.size();
    	}
	
    	@Override
    	public Object getItem(int position) {
    		return position;
    	}
	
    	@Override
    	public long getItemId(int position) {
    		return position;
    	}
	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		//Log.d(TAG, "In to get view.position="+Integer.toString(position));
    		ViewHolder holder = null;
    		if(convertView == null){
    			convertView = inflater.inflate(R.layout.note_list_item, null);
    			
    			holder = new ViewHolder();
    			holder.noteBrief = (TextView) convertView.findViewById(R.id.noteBrief);
    			holder.noteDate = (TextView) convertView.findViewById(R.id.noteDate);
    			holder.noteTime = (TextView) convertView.findViewById(R.id.noteTime);
    			//holder.noteCheck = (CheckBox) convertView.findViewById(R.id.noteCheck);
    			
    			convertView.setTag(holder);
    		}else{
    			holder = (ViewHolder) convertView.getTag();
    		}
    		
    		//Set View 
    		holder.noteBrief.setText(datalist.get(position).get("content").toString());
    		holder.noteDate.setText(datalist.get(position).get("date").toString());
    		holder.noteTime.setText(datalist.get(position).get("time").toString());
    		//holder.noteCheck.setChecked(false);
    		
    		return convertView;
    	}  
	     
    	class ViewHolder {
    		//CheckBox noteCheck;
    		TextView noteBrief;
    		TextView noteDate;
    		TextView noteTime;
    	}
    }
}
