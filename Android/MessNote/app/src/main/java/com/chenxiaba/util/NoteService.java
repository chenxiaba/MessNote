package com.chenxiaba.util;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @ClassName: NoteService
 * @Description: Offer Operates for Note
 * @author: chenxiaba
 * @date: 2014-5-5 上午12:15:18
 */
public class NoteService {
	private SQLiteHelper dbHelper;
	private static final String DB_TABLE_NAME = "note";
	
	public NoteService(Context context){
		this.dbHelper = new SQLiteHelper(context);
	}
	
	//Add new Note
	public long addNote(String content, String time){
		ContentValues cv = new ContentValues();
        cv.put("content", content);
        cv.put("timestamp", time);
        
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long row = db.insert(DB_TABLE_NAME, null, cv);
        
        return row;
	}



	//Update one Note
	public void updateNote(long id, String content, String time) {
		ContentValues cv = new ContentValues();
        cv.put("content", content);
        cv.put("timestamp", time);
        
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String where = "_id = ?";
		String[] whereValue = {Long.toString(id)};
		db.update(DB_TABLE_NAME, cv, where, whereValue);
	}

    //Get one Note
    public Cursor getNote(long id) {
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DB_TABLE_NAME+ " where _id=" + Long.toString(id),null);

        return cursor;
    }

	//All Note
	public Cursor allNotes() {
		SQLiteDatabase db = this.dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from " + DB_TABLE_NAME + " order by _id desc" ,null);
		
		return cursor;
	}


	//Delete one Note
	public void delNote(long id){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String where = "_id = ?";
		String[] whereValue = {Long.toString(id)};
		db.delete(DB_TABLE_NAME, where, whereValue);
	}

    //Used for add server new note to client
    public long addNoteByGuid(String guid, String info, String content, String time) {
        ContentValues cv = new ContentValues();
        cv.put("guid", guid);
        cv.put("info", info);
        cv.put("content", content);
        cv.put("timestamp", time);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long row = db.insert(DB_TABLE_NAME, null, cv);

        return row;
    }

    //Used for update server new note to client
    public void updateNoteByGuid(String guid, String info, String content, String time) {
        ContentValues cv = new ContentValues();
        cv.put("guid", guid);
        cv.put("info", info);
        cv.put("content", content);
        cv.put("timestamp", time);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String where = "guid = ?";
        String[] whereValue = {guid};
        db.update(DB_TABLE_NAME, cv, where, whereValue);
    }

    //Get one Note by GUID
    public Cursor getNoteByGuid(String guid) {
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DB_TABLE_NAME+ " where guid=\"" + guid + "\"", null);

        return cursor;
    }

    //All Note
    public Cursor allNoGuidNotes() {
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " +
                DB_TABLE_NAME +
                " where guid= \"\"" +
                " order by _id desc",
                null
        );

        return cursor;
    }

    //Delete one Note
    public void delNoteByGuid(String guid){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String where = "guid = ?";
        String[] whereValue = {guid};
        db.delete(DB_TABLE_NAME, where, whereValue);
    }

	//Close DB
	public void close(){
		if(dbHelper != null){
			dbHelper.close();
		}
	}
}
