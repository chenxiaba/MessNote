var data = [
{author: "Lucy", text: "Life is hard."},
{author: "Jack", text: "Try you *best* for living."}
];

var Idea = React.createClass({
	rawMarkup: function() {
		var rawMarkup = marked(
			this.props.children.toString(), 
			{sanitize: true}
			);

		return {__html: rawMarkup};
	},

	render: function() {
		return (
			<div className="idea">
			<p className="ideaAuthor">
				<span>{this.props.author}:</span>
				<span dangerouslySetInnerHTML={this.rawMarkup()} />
			</p>
			</div>
		);
	}
});

var IdeaList = React.createClass({
	render: function() {
		var ideaNodes = this.props.data.map(
			function (idea) {
				return (
        			<Idea author={idea.author}>
          				{idea.text}
        			</Idea>
      			);
    		});

		return (
			<div className="idealList">
			{ideaNodes}
			</div>
		);
	}
});

var IdeaBox = React.createClass({
	loadIdeaFromServer: function() {
		$.ajax({
			url: this.props.url,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	},

	//executes once, sets up the initial state
	getInitialState: function() {
		return {data: []};
	},

	//key to dynamic updates is call this.setState()
	componentDidMount: function() {
		this.loadIdeaFromServer();
		//use simple polling here 
		//you could easily use WebSockets or other technologies
		setInterval(this.loadIdeaFromServer, this.props.poollInterval);
	},

	render: function() {
		return (
			<div className="ideaBox">
				<h1>Ideas</h1>
				<IdeaList data={this.state.data}/>
			</div>
		);
	}
});

var IdeaForm = React.createClass({
	handleSubmit: function(e) {
		//Call preventDefault() on the event 
		//to prevent the browser's default action of submitting the form.
		e.preventDefault();
		//We use the ref attribute to assign a name to a child component 
		//and this.refs to reference the DOM node
		var author = this.refs.author.value.trim();
		var text = this.refs.text.value.trim();

		if (!text || !author) {
			return;
		};

		//TODO: send request to the server
		this.refs.author.value = '';
		this.refs.text.value = '';

		return;
	},
	render: function() {
		return (
			<form className="ideaForm" onSubmit={this.handleSubmit}>
			<input type="text" placeholder="Your name" ref="author"/>
			<input type="text" placeholder="What are thinking now..." ref="text"/>
			<input type="submit" value="Post" />
			</form>
		);
	}
});


ReactDOM.render(
	<IdeaBox url="/api/ideas" poollInterval={2000}/>,
	document.getElementById('content')
);





