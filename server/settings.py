SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'

SOCIAL_AUTH_LOGIN_URL = '/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/index/'
SOCIAL_AUTH_USER_MODEL = 'models.User'
SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
    'social.backends.github.GithubOAuth2',
)

from local_settings import *
