import sys,os
import json

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import redis
import tornadoredis

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.gen


class Application(tornado.web.Application):
    """docstring for Application"""
    def __init__(self):
        handlers = [
            (r"/", HomeHandler),
            (r"/api/ideas", IdeasHandler),
            (r"/api/idea", IdeaHandler),
        ]

        settings = dict(
            blog_title=u"MessNote",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            ui_modules={"Entry": EntryModule},
            xsrf_cookies=True,
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            login_url="/auth/login",
            debug=True,
        )

        #Act as a Message Queue
        self.tredis = tornadoredis.Client()
        self.tredis.connect()

        #Just for test
        self.init_test()

        super(Application,self).__init__(handlers, **settings)

    @tornado.gen.engine
    def init_test(self):
        
        with self.tredis.pipeline() as pipe:
            pipe.lpush("ideas", {"author":"Lucy", "text":"Work is easy then life."})
            pipe.lpush("ideas", {"author":"Jack", "text":"In life , abc is not just abc."})
            yield tornado.web.gen.Task(pipe.execute)

        print "Init redis db ok..."



class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.tredis

    def get_current_user(self):
        pass
        #user_id = self.get_secure_cookie("blogdemo_user")
        #if not user_id: return None
        #return self.db.get("SELECT * FROM authors WHERE id = %s", int(user_id))

    def any_author_exists(self):
        pass
        #return bool(self.db.get("SELECT * FROM authors LIMIT 1"))

class HomeHandler(BaseHandler):
    def get(self):
        self.render("home.html")

class IdeasHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self, id):
        print "start to get data from redis"
        ideas = yield tornado.gen.Task(self.db.lrange, "ideas:%s"%id, 0, 1)
        print "get over"
        print ideas

        ideasObj = []
        for idea in ideas:
            ideasObj.append(eval(idea))
        print json.dumps(ideasObj)

        self.set_header("Content-Type", "text/json")
        self.write(json.dumps(ideasObj))
        self.finish()

class IdeaHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self, id):
        print "start to get data from redis"
        ideas = yield tornado.gen.Task(self.db.lrange, "ideas:%s"%id, 0, 1)
        print "get over"
        print ideas

        ideasObj = []
        for idea in ideas:
            ideasObj.append(eval(idea))
        print json.dumps(ideasObj)

        self.set_header("Content-Type", "text/json")
        self.write(json.dumps(ideasObj))
        self.finish()

    def post(self):
        print "start to push data from redis"
        ideas = yield tornado.gen.Task(self.db.lpush, "ideas", )
        print "get over"
        print ideas

        ideasObj = []
        for idea in ideas:
            ideasObj.append(eval(idea))
        print json.dumps(ideasObj)

        self.set_header("Content-Type", "text/json")
        self.write(json.dumps(ideasObj))
        self.finish()


class EntryModule(tornado.web.UIModule):
    def render(self, entry):
        return self.render_string("modules/entry.html", entry=entry)


def main():
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(8000)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
