#!/usr/bin/python
# APP   : Messnote Server v0.1
# author: chenxiaba
# date  : 2015.07.11
import os
import json

#import torndb

import tornado.ioloop
import tornado.web
from  tornado.web import RequestHandler

class MainHandler(RequestHandler):
    def get(self):
        data = {}
        data["hello"] = "Hello"
        data["name"] = "weiserver"
        self.write(data)


class IdeaHandler(RequestHandler):
    """Handle msg of one user"""
    def get(self, idea_id):
        pass

    def post(self, usr_id):
        """ New idea """
        #new idea
        #idea = json.loads(self.request.body)

        #TODO: update stat table

        #self.set_header("Content-Type", "application/json")
        #self.set_status(201)
        #self.write(resp(self.__name__, True))
        pass

    def put(self, idea_id):
        """Update a messge"""
        pass

    def delete(self, idea_id):
        """Delete a idea"""


        

settings = {
    "template_path": os.path.join(os.path.dirname(__file__),"templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug": True
}   


application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/api/v1/idea/([0-9]*)", IdeaHandler)
    ], **settings)


if __name__ == "__main__":
    application.listen(80)
    print "Server is start at :80"
    tornado.ioloop.IOLoop.current().start()
