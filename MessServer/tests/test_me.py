from requests import put,get
import unittest

LOCAL_URL = 'http://localhost:5000'


class TestIdea(unittest.TestCase):
    def test_add(self):
        rsp = put(LOCAL_URL + '/idea', data={'data': "this is my first idea"}).json()
        self.assertNotEqual(rsp['result'].find('succuss'), -1, "put idea error")

    def test_get(self):
        rsp = get(LOCAL_URL + '/idea').json()
        self.assertEqual(rsp['idea'], "this is first idea")

    def tearDown(self):
        print("teardown")


if __name__ == '__main__':
    unittest.main()