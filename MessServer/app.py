#!/usr/bin/python
import os
import sqlite3
import subprocess
import json

from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

from flask_restful import Api

from idea.idea import Idea

import requests

from WXBizDataCrypt import WXBizDataCrypt

# create app
app = Flask(__name__)

# load default conf
app.config.from_object(__name__)
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'app.db'),
    SECRET_KEY='development key',
    USERNAME='lele',
    PASSWORD='default'
))
app.config.from_envvar('APP_SETTINGS', silent=True)

# RESTFUL API
api = Api(app)

# add resource
api.add_resource(Idea, '/idea')

# session map
# {third_session_key : {'openid': openid, 'session_key': session_key}}
session_map = {}

# connect db
def connect_db():
    """Connects to database"""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """Opens a new database connection"""
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()

    return g.sqlite_db


# init db
def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

def initdb_command():
    """Init the databases"""
    init_db()
    print("Init database...")

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of request"""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('note'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('note'))

@app.route("/")
def index():
    return "manage you start here"

# note 
@app.route("/note", methods=['GET', 'POST'])
def note():
    if request.method == 'GET':
        return "Get notes list."

# weixin login
WX_URL = "https://api.weixin.qq.com/sns/jscode2session\
?appid={APPID}&secret={SECRET}&js_code={JSCODE}&\
grant_type=authorization_code"

APPID = "wx1fe6482817a09f63"
SECRET = "bfe316d1296bd35102ff420e7bfeb963"


def gen_third_session(openid, session_key):
    """
    :param openid:
    :param session_key:
    :return: 168 third session key
    """
    return str(subprocess.check_output("head -n 80 /dev/urandom |tr -dc A-Za-z0-9", shell=True)[0:168])


def wexin_login(code):
    """
    :param code:
    :return: third_session_key
    """

    url = WX_URL.format(APPID=APPID, SECRET=SECRET, JSCODE=code)
    print(url)
    r = requests.get(url, verify='/etc/ssl/certs', timeout=5)
    print(r.json())
    json = r.json()

    openid = json['openid']
    session_key = json['session_key']

    third_session_key = gen_third_session(openid, session_key)

    # save into db
    session_map[third_session_key] = {'openid': openid,
            'session_key': session_key}

    return third_session_key


# login
@app.route("/v1.0/weixin/login", methods=['POST'])
def wx_login():
    data = request.get_json()
    print(data)

    code = data['code']

    # login
    third_session_key = wexin_login(code)
    return json.dumps({'third_session_key': third_session_key})


# check session
@app.route("/v1.0/weixin/check", methods=['POST'])
def wx_check():
    data = request.get_json()
    print(data)

    code = data['code']

    if code in session_map:
        return json.dumps({'status': False})
    else:
        return json.dumps({'status': True})


"""
{"userInfo":
 {"province": "Guangdong", "city": "Shenzhen", "language": "zh_CN", "avatarUrl": "http://wx.qlogo.cn/mmopen/",
 "gender": 1, "country": "CN", "nickName": "\u5f20\u4e50\u4e50"
 },

 "iv": "ylqHXNtQzt8whM2r8fpoqg==",
 "encryptedData": "",
 "signature": "",
 "encryptData": "=",
 "errMsg": "getUserInfo:ok",
 "rawData": "{\"nickName\":\"\u5f20\u4e50\u4e50\",\"gender\":1,\"language\":\"zh_CN\",\"city\":\"Shenzhen\",\"province\":\"Guangdong\",\"country\":\"CN\",\"avatarUrl\":\"\"}"
 }
"""
# Decrypt UserInfo
@app.route("/v1.0/weixin/userinfo", methods=['POST'])
def wx_UserInfo():
    data = request.get_json()
    print(data)

    res = data['res']

    # iv = res['iv']
    # encryptedData = res['encryptedData']
    # app_session_key = res['app_session_key']
    # 
    # session_key = session_map[app_session_key]['session_key']
    # #Get session key
    #
    # pc = WXBizDataCrypt(APPID, session_key)
    #
    # print pc.decrypt(encryptedData, iv)

    print(json.dumps(res))

    return json.dumps({'status': True})


# run app
if __name__ == "__main__":
    app.run()
