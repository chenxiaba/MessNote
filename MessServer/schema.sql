drop table if exists notes;
drop table if exists ideas;

create table notes (
    id integer primary key autoincrement,
    title text not null,
    content text not null
);

create table ideas (
    id integer primary key autoincrement,
    content text not null,
    datetime text not null
);
