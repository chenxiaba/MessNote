var util = require('util.js')

var TAG = "_"

//保存笔记
function trySaveNote(content, noteId, unionid){
    //no content
    console.log("trySaveNote: content is: " + content);
    if (String(content).length == 0 ){
        //删除笔记
        if (noteId.length > 0) {
          console.debug("noteId is not null, but content is null ,so del the note.\n");

          delNote(noteId);
        } 
        return;
    }

    //Gen noteId 
    if (noteId.length == 0) {
      var today = new Date();
      noteId = unionid + "_" + String(today.getTime());
      console.debug("add new note: " + noteId);
    }
    else {
      console.debug("update old note");
    }

    saveNote(noteId, content);
}

//保存笔记
function saveNote(noteId, content) {
  saveNoteMeta(noteId, content);
  saveNoteContent(noteId, content);
  console.debug("Save note succuss!\n");
}

//删除笔记
function delNote(noteId){
  delNoteMeta(noteId);
  delNoteContent(noteId);
  console.debug("Del note succuss!\n");
}


//保存元数据
function saveNoteMeta(noteId, content){
  console.log("saveNoteMeta");

  var now = new Date();
  var noteContentId = TAG + noteId

  //metadata
  var noteMeta = {
    content: noteContentId,
    meta: {
      date: util.formatJustDate(now),
      time: util.formatJustTime(now),
      summary: String(content).slice(0,48)
    }
  }

  console.log(JSON.stringify(noteMeta));

  //save noteId
  var noteIdList = wx.getStorageSync('noteIdList') || []
  noteIdList.unshift(noteId)
  wx.setStorageSync('noteIdList', noteIdList)

  //save metadata
  wx.setStorageSync(noteId, noteMeta);

  console.debug("Save notes meta over\n")
}


//保存内容
function saveNoteContent(noteId, content) {
  var noteContentId = TAG + noteId;
 
  //content
  var noteData = {
    content: content
  }

  console.log(JSON.stringify(noteData));

  //save content
  wx.setStorage({
    key: noteContentId,
    data: noteData,
    success: function(res){
      // success
      console.debug("Save note content succuss!" + noteContentId)
    },
    fail: function() {
      // fail
      console.error("Set note content failed!" + noteContentId);
    },
    complete: function() {
      // complete
      console.debug("setStorage complete!");
    }
  });

  console.debug("Save note content succuss!\n");
}


//删除元数据
function delNoteMeta(noteId){
  //删除元数据
  var notes = wx.getStorageSync('notes');
  console.debug(notes);

  for (var i=0;i<notes.length;i++) {
    if (notes[i].id == noteId) {
      notes.splice(i, 1);
      break;
    }
  }

  //保存
  wx.setStorageSync('notes', notes);
  console.debug(wx.getStorageSync('notes'));

  console.debug("del note meta over.\n");
}


//删除内容
function delNoteContent(noteId) {
  var noteContentId = TAG + noteId;

  wx.clearStorageSync(noteContentId);

  console.log("Del " + (TAG + noteId) + "over!");
}


//获取笔记的内容
function getNoteContent(noteId) {
  var noteContentId = TAG + noteId

  //从本地缓存获取
  try {
    var value = wx.getStorageSync(noteContentId)
    if (value) {
      // Do something with return value
      return value.content;
    }
  } catch (e) {
    // Do something when catch error
    console.error("Can't find note content with " + noteContentId);
  }
}

//从本地获取笔记notes列表
function getNotesSummary(){
  try{
    console.debug("Start to get notes info.\n");

    var notes = new Array()

    //save noteIdList
    var noteIdList = wx.getStorageSync('noteIdList');
    console.debug(JSON.stringify(noteIdList) + "length:" + noteIdList.length);

    //get noteMeta
    for(var i=0; i< noteIdList.length; i++)
    {
      var noteMeta = getNoteMetaById(noteIdList[i]);
      notes.push(noteMeta);
    }

    console.debug("Get notes summary ok:\n" + JSON.stringify(notes));

    return notes;
  } catch (e) {
    //Do nothing
  }
}

//get noteMeta
function getNoteMetaById(noteId)
{
  var noteMeta = wx.getStorageSync(noteId);

  console.debug("Get noteMeta ok: " + noteId);
  return noteMeta;
}

module.exports = {
trySaveNote: trySaveNote,
getNoteContent: getNoteContent,
getNotesSummary: getNotesSummary
}
