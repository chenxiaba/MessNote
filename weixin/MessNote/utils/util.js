function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

//hashcode
function hashCode(str){
  var h = 0, off = 0;
  var len = str.length;
  for(var i = 0; i < len; i++){
      h = 31 * h + str.charCodeAt(off++);
  }
  return h;
}

function formatJustDate(date) {
  var formateDate = formatTime(date);
  var array = formateDate.split(" ");
  return array[0];
}

function formatJustTime(date){
  var formateDate = formatTime(date);
  var array = formateDate.split(" ");
  return array[1];
}

module.exports = {
  formatTime: formatTime,
  formatJustDate: formatJustDate,
  formatJustTime: formatJustTime,
  hashCode: hashCode
}
