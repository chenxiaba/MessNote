//app.js
App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    //读取用户信息
    this.globalData.userInfo =  wx.getStorageSync('userInfo');

    console.debug("UserInfo: " + JSON.stringify(this.globalData.userInfo));
  },

  getUserInfo:function(cb){
    if(this.globalData.userInfo){
      typeof cb == "function" && cb(this.globalData.userInfo)
    }

     var app_session_key = "";

    //尝试获取会话id
    try {
      app_session_key = wx.getStorageSync('app_session_key');
    } catch (e) {
      // Do something when catch error
      console.debug("start to wx.login");
    }

    //如果已经存在存在，则检查是否过期
    check_and_login(app_session_key);
  },

  globalData:{
    userInfo:null
  }
})


//检查session是否存在或者过期
//如果过期，则登录；如果不存在不存在，直接登录
function check_and_login(app_session_key)
{
  console.debug("start to check session");

  if (app_session_key.length > 0) {
      // Do something with return value
      console.debug("app has logined..");

      //发起网络请求
      wx.request({
        url: 'https://api.messnote.com/v1.0/weixin/check',
        data: {
          code: app_session_key
        },
        header: {
          'content-type': 'application/json'
        },
        method: 'POST',

        success: function(res) {
          console.debug("wx.request wxCheck ok!" + JSON.stringify(res.data));

          var needLogin = null;

          needLogin = res.data['status'];

          if (needLogin == null) {
            console.warn("wxCheck can't find status flag.");
            return;
          }

          if (needLogin){
            console.debug("wxCheck over, need to relogin.");
            //清理当前到到session_key
            wx.clearStorageSync('app_session_key');

            //session过期重新登录
            wxlogin();
            return;
          }

          console.debug("wxCheck over, no need to login.");
        },

        fail: function(){
          console.warn("wx.request wxCheck failed");
        },
      });
  }
  else
  {
    //直接登录
    wxlogin();
  }
}

//登陆
function wxlogin()
{
  console.debug("start wxlogin");

  //调用登录接口
    wx.login({
      success: function (res) {
        if (res.code) {
          console.debug("wx.login ok!" + res.code);

          //提交到服务器获取session_key
          getAppSessionKey(res)

          //获取用户信息
          getUserInfo();
        }
      }, // end success

      fail: function (){
        console.warn("wx.login failed");
      }
    });
}


//获取用户信息
function getUserInfo()
{
  console.debug("start to get userInfo");

  wx.getUserInfo({
    success: function(res){
      // success
      console.debug("getUserInfo ok!" + JSON.stringify(res));

      //Update GlobalData UserInfo
      wx.setStorageSync('userInfo', res.userInfo);

      //提交到服务器
      getEncryptInfo(res);
    },

    fail: function() {
      // fail
      console.warn("getUserInfo failed!");
    },
    complete: function() {
      // complete
    }
  });
}

//请求服务器  获取session key
function getAppSessionKey(res)
{
  console.debug("start wx.request wxLogin");

  //发起网络请求
  wx.request({
    url: 'https://api.messnote.com/v1.0/weixin/login',
    data: {
      code: res.code
    },
    header: {
      'content-type': 'application/json'
    },
    method: 'POST',
    success: function(res) {
      console.debug("getAppSessionKey over!" + JSON.stringify(res.data));
      
      var app_session_key = res.data['third_session_key']

      if (app_session_key != null)
      {
        // 存储到本地缓存
        wx.setStorageSync('app_session_key', app_session_key);
        return;
      }

      console.warn("wx.request wxlogin result can't find third_session_key");
      },

      fail: function(){
        console.warn("wx.request wxlogin failed");
      }
  });
}

//请求服务器 获取用户加密信息
function getEncryptInfo(res)
{
  console.debug("start wx.request wxUserInfo");

  //发起网络请求
  wx.request({
    url: 'https://api.messnote.com/v1.0/weixin/userinfo',
    data: {
      res: res
    },
    header: {
      'content-type': 'application/json'
    },
    method: 'POST',
    success: function(res) {
      console.debug("wx.request  wxUserInfo ok! " + JSON.stringify(res.data));
      // 相关信息存储到本地缓存
      //wx.setStorageSync('app_session_key', res.data['third_session_key']);
      },

      fail: function(){
        console.warn("wx.request wxUserInfo failed");
      }
  });
}