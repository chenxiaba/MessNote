var util = require('../../utils/util.js')
var note_model = require('../../utils/note_model.js')

//获取应用实例
var app = getApp()

Page({
    data: {
        note :{
            date:"",
            time: ""
        },
        id : 1001
    },

    globalData:{
        unionId:null,
        content: ""
    },

    //页面加载
    onLoad: function () {
    var now = new Date()

    //初始化时间戳即可
    this.setData({
      note: {
          date: util.formatJustDate(now),
          time: util.formatJustTime(now)
      }
    });

    //调用应用实例的方法获取全局数据
    var that = this;
    app.getUserInfo(function(userInfo){
      //更新数据
        that.globalData.unionId = userInfo.unionId
      });

    console.log("unionid is %s" % that.globalData.unionId)
    },
    onHide:function(){
        // 页面隐藏
        console.log("onHide: note.js");
        if (this.globalData.content.length != 0){
            note_model.trySaveNote(this.globalData.content, "", this.globalData.unionId);
        }
    },


    onUnload:function(){
        // 页面关闭
        console.log("onUnload: note.js: " + this.globalData.content);
        if (this.globalData.content.length != 0){
            note_model.trySaveNote(this.globalData.content, "", this.globalData.unionId);
        }
    },

    //失去焦点的时候时候，保存编辑框里的文字全局变量
    bindTextAreaBlur: function(e) {
        console.log(e.detail.value);
        this.globalData.content = e.detail.value;
    },
})