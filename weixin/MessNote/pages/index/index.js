// TODO:
// 1. 登录放到app启动时
// 2. 支持动态更新内容  
// 3. 支持修改笔记
// 4. 支持远程服务器保存笔记
// 5. 支持上传图片
// 6. 支持图文混排
// 7. 界面显示优化

//index.js
var note_model = require('../../utils/note_model.js')

//获取应用实例
var app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    notes: []
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  //添加新想法
  addNewNote: function () {
    console.log('addNewNote')
    wx.navigateTo({
      url: '../note/note'
    })
  },

  //初始化加载
  onLoad: function () {
    console.log('onLoad');

    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    })

    //初始化notelist
    //TODO: 分页加载 
    that.setData({
        notes: note_model.getNotesSummary()
      });

    console.debug(JSON.stringify(this.data.notes));
  },
  
})

//初始化多时候，先从服务器获取最新多文件清单（默认10个）
